odoo.define('transport.analytics_3_widget', function (require) {
    "use strict";

    var AbstractField = require('web.AbstractField');
    var core = require('web.core');
    var registry = require('web.field_registry');
    let utils = require('web.utils');
    var qweb = core.qweb;
    var _t = core._t;

    let BarChartWidget_2 = AbstractField.extend({
        className: "oe_line_chart_value d-block d-inline-block",
        jsLibs: [
            '/web/static/lib/Chart/Chart.js',
        ],
        //Render this chart in odoo front

        _renderReadonly: function () {
            var self = this;
            this._rpc({

                model: 'hack.company',
                method: 'analytics_of_incomes_by_day',
                args: [self.record.data.id],
            }).then(response => {
                let graph_line = {
                    type: 'pie',
                    data: {
                        labels: ['Nur-Sultan', 'Almaty', 'Shymkent', 'Kokshetau', 'Taraz', 'Kyzylorda'],

                        datasets: [
                            {
                                data: [87, 12, 32, 12, 45, 10],
                                backgroundColor: ["#390099", "#9e0059", "#ff0054", "#db222a", "#ff5400", "#ffbd00"],
                            }
                        ]
                    },
                };

                //variables
                this.$canvas = $('<canvas/>');
                this.$el.append(this.$canvas);
                this.$el.attr('style', this.nodeOptions.style);
                this.$el.css({position: 'relative'});
                let context = this.$canvas[0].getContext('2d');
                this.chart = new Chart(context, graph_line);
                let $value = $('<span class="oe_line_chart_value">');
                $value.css({
                    'text-align': 'center',
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    bottom: '6px',
                    'font-weight': 'bold'
                });
                this.$el.append($value);
            })


        },

        _render: function () {
            return this._renderReadonly();
        },

    });

    registry
        .add('analytics_3_widget', BarChartWidget_2);

    return BarChartWidget_2;
})