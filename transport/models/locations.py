# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.modules import get_module_resource
import base64


class Fermers(models.Model):
    _name = "hack.location"
    _description = "Delivery's model"

    name = fields.Char(string="Location name")
    coordinate_X = fields.Float(string="Coordinate X")
    coordinate_Y = fields.Float(string="Coordinate Y")
