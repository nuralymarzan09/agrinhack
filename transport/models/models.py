# -*- coding: utf-8 -*-
import base64

from odoo import models, fields, api
from odoo.modules import get_module_resource


class Fermers(models.Model):
    _name = "hack.fermer"
    _description = "Fermer's model"

    name = fields.Char(string="Name")
    city = fields.Char(string="Location city")
    address = fields.Char(string="Current address")
    phone = fields.Char(string="Telephone")
    email = fields.Char(string="Email")
    partner_id = fields.Many2one('res.partner', 'Student/Faculty')
    current_stock_id = fields.Many2one('hack.order', string="Stock id")

    @api.model
    def _default_image(self):
        image_path = get_module_resource('transport', 'static/src/img',
                                         'image_1.png')
        return base64.b64encode(open(image_path, 'rb').read())

    image_1920 = fields.Image(string='Photo', default=_default_image)

    @api.model
    def create(self, vals):
        # user_group = self.env.ref("openeducat_core_upd.group_op_student")
        res = super().create(vals)
        vals = {
            'name': res.name,
            'partner_id': res.partner_id.id,
            'login': res.email,
            # 'is_student': True,
            'groups_id': [(4, self.env.ref('transport.group_hack_customer').id)],
            'tz': self._context.get('tz'),
        }
        user_id = self.env['res.users'].create(vals)
        res.user_id = user_id
        print(res.user_id)
        return res
