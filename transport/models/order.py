import qrcode
from odoo import models, fields, api
from odoo.modules import get_module_resource
import base64
from io import BytesIO


class Fermers(models.Model):
    _name = "hack.order"
    _description = "Order's model"

    farmer_id = fields.Many2one('hack.fermer', string="Name of receiver")
    user_id = fields.Many2one('res.users', 'Sender', compute='_get_current_user')
    delivery_company_id = fields.Many2one('hack.delivery', string="Delivery")
    product_ids = fields.Many2many('hack.product', string="Products in list")
    # count_product = fields.Integer(string="Product count")
    date_1 = fields.Datetime(string="Ordering date")
    date_2 = fields.Datetime(string="Дата отправки")
    date_3 = fields.Datetime(string="Estimated delivery time")
    date_4 = fields.Datetime(string="Real Delivery date")
    date_5 = fields.Datetime(string="All done")
    logistic_centre = fields.Many2one('hack.delivery', string="Logistic Centre")
    location_A = fields.Many2one('hack.location', string="Location A")
    location_B = fields.Many2one('hack.location', string="Location B")
    current_latitude = fields.Float(string="Current latitude")
    current_longitude = fields.Float(string="Current longitude")
    status = fields.Selection([
        ('in_draft', 'Draft'),
        ('level_2', 'Оформить заказ'),
        ('level_3', 'Отправлен на рассмотрение'),
        ('level_4', 'Подписание договора лиц 1'),
        ('level_5', 'Подписание договора лиц 2'),
        ('level_6', 'Подписен договор и одобрен'),
        ('level_7', 'В процессе отгрузки'),
        ('level_8', 'Отгружен'),
        ('level_9', 'В пути'),
        ('level_10', 'Доставлен на точку B'),
        ('level_11', 'Принят клиентом'),
        ('level_12', 'Оплачен'),
        ('level_13', 'В прочессе оплаты'),
        ('level_14', 'Выполнен'),
        ('level_15', 'Отмена заказа')
    ], string="Status", default="in_draft")
    sign_1_dog = fields.Boolean(string="Signed by client")
    sign_2_dog = fields.Boolean(string="Signed by logistics")
    sign_3_dog = fields.Boolean(string="Signed by shop")
    make_payment = fields.Boolean(string="Make payment")
    payment_type = fields.Selection([
        ('1', 'Наличная оплата'),
        ('2', 'Погашение по определенному времени')
    ])
    total_cost = fields.Float(string="Total cost", compute='_compute_result')
    color = fields.Integer('Color Index', compute='set_kanban_color')
    client_sign = fields.Binary(attachment=True, store=True)
    logistic_sign = fields.Binary(attachment=True, store=True)
    qr_code = fields.Binary("QR Code", attachment=True, store=True)

    def set_kanban_color(self):
        for col in self:
            color = 0
            if col.status == 'in_draft':
                color = 2
            elif col.status == 'level_2' or col.status == 'level_2':
                color = 7
            elif col.status == 'level_4' or col.status == 'level_4':
                color = 3
            col.color = color

    def _get_current_user(self):
        for r in self:
            r.user_id = self.env.user

    def move_status_1(self):
        for rec in self:
            rec.status = "level_2"

    def move_status_2(self):
        for rec in self:
            rec.status = "level_3"

    def move_status_3(self):
        for rec in self:
            rec.status = "level_4"

    def move_status_4(self):
        for rec in self:
            rec.status = "level_5"

    def move_status_5(self):
        for rec in self:
            rec.status = "level_6"

    def move_status_6(self):
        for rec in self:
            rec.status = "level_7"

    def move_status_7(self):
        for rec in self:
            rec.status = "level_8"

    def _compute_result(self):
        for rec in self:
            for i in rec.product_ids:
                mult = i.cost * i.count_num
                rec.total_cost += mult
            if rec.logistic_centre:
                rec.total_cost = rec.total_cost + rec.logistic_centre.cost_for_platon
            return rec.total_cost

    def move_status_8(self):
        qr = qrcode.QRCode(
            version=1,
            error_correction=qrcode.constants.ERROR_CORRECT_L,
            box_size=10,
            border=4
        )
        qr.add_data(self.id)
        qr.make(fit=True)
        img = qr.make_image()
        temp = BytesIO()
        img.save(temp, format="PNG")
        qr_image = base64.b64encode(temp.getvalue())
        self.qr_code = qr_image
