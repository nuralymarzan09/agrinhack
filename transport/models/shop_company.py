# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.modules import get_module_resource
import base64


class Fermers(models.Model):
    _name = "hack.shop.company"
    _description = "Company's shop model"

    name = fields.Char(string="Name")
    cost = fields.Integer(string="Cost")  # Minimal cost from provider shops
    main_shop_id = fields.Many2one('hack.product', string="Main product")
    count_in_stock = fields.Integer(string="Number in stock")
    company_id = fields.Many2one('hack.company', string="Company name")
    category = fields.Many2one('hack.product.category', string="Product category")
    description = fields.Text(string="Description")

    @api.model
    def _default_image(self):
        image_path = get_module_resource('transport', 'static/src/img',
                                         'image_1.png')
        return base64.b64encode(open(image_path, 'rb').read())

    image_1920 = fields.Image(string='Photo', default=_default_image)
