# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.modules import get_module_resource
import base64
from geopy.geocoders import Nominatim
from geopy import distance

AVAILABLE_PRIORITIES = [
    ('0', 'Very Low'),
    ('1', 'Low'),
    ('2', 'Normal'),
    ('3', 'High'),
    ('4', 'Very High')]


class Fermers(models.Model):
    _name = "hack.delivery"
    _description = "Delivery's model"

    name = fields.Char(string="Delivery company name")
    type_logistic = fields.Selection([
        ('type_1', 'Перевозчики'),
        ('type_2', 'Экстремальные перевозки')
    ], string="Тип логистической компании")
    cost_per_km = fields.Float(string="Cost for km", default=10.1)
    city_1 = fields.Char(string="City 1")
    city_2 = fields.Char(string="City 2")
    total_distance = fields.Float(string="Total distance between cities")
    cost_for_platon = fields.Float(string="Cost for total delivery(Platon)")
    average_time_for_delivery = fields.Char(string="Average time for delivery")
    set_priority = fields.Selection(AVAILABLE_PRIORITIES, select=True, string="Give review")
    garanties = fields.Boolean(string="Наличие сертификатов страхования грузов и ответственности логиста.")
    garanties_1 = fields.Boolean(
        string="Наличие крупных, чаще, иностранных, компаний в пуле Клиентов (известно, что зачастую отбор "
               "подрядчиков на западных предприятиях проходит по более жёстким, формализованным процедурам).")
    garanties_2 = fields.Boolean(
        string="Членство в профильных Ассоциациях (например, АСМАП - Ассоциация международных автомобильных "
               "перевозчиков; asmap.ru).")

    @api.onchange('city_1', 'city_2')
    def get_location(self):
        geolocator = Nominatim(user_agent="geoapiExercises")
        place_1 = geolocator.geocode(self.city_1)
        place_2 = geolocator.geocode(self.city_2)
        print(place_1)
        print(place_2)
        loc1_lat, loc1_lon = place_1.latitude, place_1.longitude
        loc2_lat, loc2_lon = place_2.latitude, place_2.longitude
        location_1 = (loc1_lat, loc1_lon)
        location_2 = (loc2_lat, loc2_lon)
        self.total_distance = distance.distance(location_1, location_2).km
        print(distance.distance(location_1, location_2).km, "kms")
        self.cost_for_platon = int(self.cost_per_km * self.total_distance)
