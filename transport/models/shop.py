# -*- coding: utf-8 -*-
import base64
from datetime import date

from odoo import models, fields, api
from odoo.modules import get_module_resource


class Fermers(models.Model):
    _name = "hack.product"
    _description = "Shop"

    name = fields.Char(string="Name")
    category = fields.Many2one('hack.product.category', string="Product category")
    description = fields.Text(string="Description")
    cost = fields.Integer(string="Cost", compute="_compute_cost")  # Minimal cost from provider shops
    count_num = fields.Integer(string="Count number")
    total_1 = fields.Integer(string="Total cost", compute='_compute_total_1')
    company_id = fields.Many2one('hack.company', string="Company name")
    recommendation_ids = fields.One2many('hack.shop.company', 'main_shop_id', string="Recommendations")
    status = fields.Selection([
        ('In progress', 'In progress'),
        ('Added to cart', 'Added to cart')
    ], string="Status", default='In progress')
    user_id = fields.Many2one('res.users', compute='_get_current_user')

    @api.model
    def _default_image(self):
        image_path = get_module_resource('transport', 'static/src/img',
                                         'image_1.png')
        return base64.b64encode(open(image_path, 'rb').read())

    image_1920 = fields.Image(string='Photo', default=_default_image)

    def _get_current_user(self):
        for r in self:
            r.user_id = self.env.user

    def _compute_cost(self):
        for rec in self:
            costs = []
            if rec.recommendation_ids:
                for i in rec.recommendation_ids:
                    costs.append(i.cost)
                rec.cost = min(costs)
                return rec.cost
            else:
                rec.cost = 0
                return rec.cost

    @api.onchange('company_id')
    def onchange_cost(self):
        for rec in self:
            if rec.company_id:
                product_obj = self.env['hack.shop.company']
                product_id = product_obj.search(
                    ['&', ('company_id', '=', self.company_id.id), ('main_shop_id', '=', self._origin.id)])
                for i in product_id:
                    rec.cost = i.cost

    def add_cart(self):
        for rec in self:
            rec.status = "Added to cart"
            # order_obj = self.env['hack.order']
            # order_id = order_obj.search(
            #     ['&', ('user_id', '=', self.user_id.id), ('date_1', '=', date.today())])
            # print(len(order_id))
            # if len(order_id) == 0:
            #     vals = {
            #         'user_id': self.user_id,
            #         'date_1': date.today()
            #     }
            #     order_id_1 = self.env['hack.order'].create(vals)
            #     print(order_id_1)

    def _compute_total_1(self):
        for rec in self:
            rec.total_1 = rec.count_num * rec.cost
            return rec.total_1
