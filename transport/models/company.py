# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.modules import get_module_resource
import base64


class Fermers(models.Model):
    _name = "hack.company"
    _description = "Company's model"

    name = fields.Char(string="Company name")
    city = fields.Char(string="Company city")
    address = fields.Char(string="Company address")
    number = fields.Char(string="Company telephone")
    email = fields.Char(string="Company email")
    iin = fields.Char(string="IIN/BIN")
    link = fields.Char(string="Company website")
    partner_id = fields.Many2one('res.partner', 'Student/Faculty')
    analytics_1_widget_field = fields.Boolean(string="Graph")
    analytics_2_widget_field = fields.Boolean(string="Graph")
    analytics_3_widget_field = fields.Boolean(string="Graph")

    # stock_ids = fi

    @api.model
    def _default_image(self):
        image_path = get_module_resource('transport', 'static/src/img',
                                         'image_1.png')
        return base64.b64encode(open(image_path, 'rb').read())

    image_1920 = fields.Image(string='Photo', default=_default_image)

    def get_company_stack(self):
        self.ensure_one()
        return {
            'type': 'ir.actions.act_window',
            'name': 'Stock',
            'view_type': 'form',
            'view_id': False,
            'res_model': 'hack.shop.company',
            'view_mode': 'tree',
            'domain': [('company_id', '=', self.id)],
            'context': "{'create': True}",
        }

    @api.model
    def create(self, vals):
        # user_group = self.env.ref("openeducat_core_upd.group_op_student")
        res = super().create(vals)
        vals = {
            'name': res.name,
            'partner_id': res.partner_id.id,
            'login': res.email,
            # 'is_student': True,
            'groups_id': [(4, self.env.ref('transport.group_hack_seller').id)],
            'tz': self._context.get('tz'),
        }
        user_id = self.env['res.users'].create(vals)
        res.user_id = user_id
        print(res.user_id)
        return res

    def analytics_of_incomes_by_day(self):
        result = [{
            "income": [103, 309, 123, 121, 345, 675, 122],
            "days": ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
        }]
        return result
