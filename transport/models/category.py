# -*- coding: utf-8 -*-


from odoo import models, fields, api
from odoo.modules import get_module_resource
import base64


class Fermers(models.Model):
    _name = "hack.product.category"
    _description = "Product's category model"

    name = fields.Char(string="Category name")
